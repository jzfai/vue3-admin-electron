# vue3-admin-electron
 
the electron version of vue3-admin-plus , provide enterprise-class using demo

suggestion the Node.js >= 16.0.0。


## Documents

- [Official Documentation](https://github.jzfai.top/vue3-admin-doc/)

- [中文官网](https://github.jzfai.top/vue3-admin-doc/zh/)



## Online experience

[Access address](https://github.jzfai.top/vue3-admin-plus)

[国内体验地址](https://github.jzfai.top/vue3-admin-plus)



## Related items

The framework is available in js,ts, plus and electron versions
- js version：[vue3-admin-template](https://github.com/jzfai/vue3-admin-template.git) -- basic version
- ts version：[vue3-element-ts](https://github.com/jzfai/vue3-admin-ts.git)
- ts version for plus：[vue3-element-plus](https://github.com/jzfai/vue3-admin-plus.git)


## Build Setup

```bash
# clone the project
git clone https://github.com/jzfai/vue3-admin-electron.git

# enter the project directory
cd vue3-admin-electron

#notes: this is issue of build using pnpm , so recommend using yarn  
yarn add 

# develop web
yarn run dev


# develop electron
yarn run electron:dev

```

## Build

```bash
# build for web
yarn run build

# build for electron dir
yarn run electron:dir

# build for electron
yarn run electron:build
```



## Browsers support

Note: Vue3 is not supported the Internet Explorer


## Discussion and Communication
[WeChat group](https://github.jzfai.top/file/images/wx-groud.png)



